# 0.14.0

* added env var WBS_ENABLE_ANNOT_CASE_DATA_PARTITIONING for EMP-0100

# 0.13.12

* fixed validate post data

# 0.13.11

* fix app model

# 0.13.9

* added slide regions endpoint for Workbench v3 API

# 0.13.8

* removed unnecessary job locking for wsi type, because it is also not present in the workbench daemon for preprocessing jobs

# 0.13.6 & 7

* renovate

# 0.13.5

* bugfix for post data validation

# 0.13.3

* fixed tag in API docs

# 0.13.1 - 2

* renovate

# 0.13.0

* added indication and procedure tags to case model to support EMP-0105

# 0.12.1

* updated RSA keys dir for docker compose

# 0.12.0

* added pixelmap data accept-encondig and content-encoding headers to support EMP-0107
* added pixelmap info endpoint to support EMP-0108

# 0.11.19

* refactored class locking in v3 API

# 0.11.18

* renovate

# 0.11.17

* rework multi-user data sharing in v3
* added ENV WBS_DISABLE_MULTI_USER, if true only data and jobs from current scope are accessable
* added new endpoint in v3 scopes API
  * GET `/{scope_id}/scopes`

# 0.11.11 - 0.11.16

* renovate

# 0.11.10

* removed app_id from empaia integration user_examination_hook

# 0.11.9

* added more customizable auth hook mechanism
* using FastAPI lifespan instead of startup event
* updated pydantic settings definition

# 0.11.8

* small validation logic fix for post scope data

# 0.11.7

* optimized query validation in v2 and v3

# 0.11.6

* renovate

# 0.11.5

* bugfixes for post data validation

# 0.11.3 & 0.11.4

* renovate

# 0.11.2

* renovate

# 0.11.2

* renovate

# 0.11.1

* fixed error in v3 scopes pixelmap bulk get endpoint

# 0.11.0

* added pixelmaps to v3 Scopes API

# 0.10.20

* renovate

# 0.10.19

* downgraded pydantic and pinned version to <2.5

# 0.10.18

* renovate

# 0.10.17

* renovate

# 0.10.16

* renovate

# 0.10.15

* updated submodules

# 0.10.14

* updated daemon for jes job scheduling

# 0.10.13

* renovate

# 0.10.12

* fixed IDMS default url

# 0.10.11

* updated v3 scope API OpenAPI

# 0.10.10

* updated py_ead_validation submodule
* added check in v3 scope API to reject queries that filter for jobs and creators to support multiple users

# 0.10.9

* renovate

# 0.10.8

* catch fastapi's HTTPException explicitly in wbd

# 0.10.7

* quickfix: trim client_id to client_id[:50] in jobs and examination creation by WBD

# 0.10.6

* fix for new tag group `scanners` in v3 /tags route

# 0.10.5

* renovate

# 0.10.4

* added settings for http_client:
  * connection_chunk_size
  * http_client_timeout
  * request_timeout
  * connection_limit_per_host

# 0.10.3

* renovate

# 0.10.2

* bugfix for empty `rewrite_url_in_wellknown` env variable

# 0.10.1

* added ENV variables to disable input and/or output validation in WBD

# 0.10.0

* update submodules
* migrate to pydantic v2
* update dependencies

# 0.9.17

* renovate

# 0.9.16

* remove v1 API

# 0.9.15

* renovate

# 0.9.14

* performance improvements for wbs v3 cases route

# 0.9.13

* increase frontend token expiration, because browser caching does not work with strict CSP

# 0.9.12

* added env variable to configure job token experation

# 0.9.11

* renovate

# 0.9.10

* minor fix in examinations/apps route

# 0.9.9

* fetch all aggregated apps once in examination/apps route in v2 API

# 0.9.8

* renovate

# 0.9.7

* renovate and changed repo maintainer

# 0.9.6

* updated v3 scope data query validation

# 0.9.5

* added ENV to enable EATS_MODE in WBD when running apps without JES

# 0.9.4

* renovate

# 0.9.3

* updated Py EAD Validation

# 0.9.2

* updated for changed v3 models

# 0.9.1

* performance fix for `cases/{case_id}/apps`route in WBS2/3

# 0.9.0

* removed auth v1 compatibility

# 0.8.8

* fix WBSv3 same behaviour as 0.8.7
  * was only done for v1 and v2. v3 wsa somehow missed

# 0.8.7

* add ENV WBS_AAA_SERVICE_API_VERSION
  * v1: ""
  * v2: "v2"

# 0.8.6

* added worker-src to v2 / v3 csp default settings

# 0.8.5

* again, refactor examination close

# 0.8.4

* 1) fix 500 on close examination for reports with missing output
* 2) remove unnecessary trial of locking inputs for reports on close examination
  * reports may not have inputs anyways
    * EDIT: RE-ENABLED as reports MAY have inputs
* 3) checkout recent definitions repo incl latest EADv3 (with reports as job mode)
* 4) appropriate stoping of containerized jobs on close examination
* 5) setting NONE and ASSEMBLY contianerized jobs (JES doesnt know them yet) to ERROR in JS on examination close

# 0.8.3

* rmeoved duplicate job validations on scopes API (is now done by WBD)

# 0.8.2

* renovate

# 0.8.1

* fixed HTTP 500 error when sending non UUID4 examination ID

# 0.8.0

* add job validation to daemon tasks

# 0.7.11

* fix allow origins settings for frontends route

# 0.7.10

* dirty fix for collections in job validation (will be removed when async job validation is implemented)

# 0.7.9

* v3: performance improvements due to new examination query route
* v1/2/3: mps_connector refacotring (shared)
* refactoring v3 custom app mode (api_version -> app_version)

# 0.7.8

* renovate

# 0.7.7

* reworked preprocessing trigger route

# 0.7.6

* fix extending preprocessing trigger with app

# 0.7.5

* update models repo and examination query
* use custom preprocessing trigger

# 0.7.4

* adapted WBS to updated MDS and fixed some API issues

# 0.7.3

* bugfix mps query

# 0.7.2

* refactor
  * apps in examination (v1, v2) should now display all apps including old versions)
  * added disable_csp
  * close examination (v3)

# 0.7.1

* fixing examinations in v1 / v2 mixing v1 / v2 apps
* add scopes put output (postprocessing jobs)
* add scopes finalize job (postprocessing jobs)
* report mode functionality

# 0.7.0

* v3 api

# 0.6.19

* renovate

# 0.6.18

* reverted logging of frontend resources

# 0.6.17

* log error when streaming frontend resource

# 0.6.16

* reverted slide info model usage in v1 and v2
* updated models
* updated for changed sender auth

# 0.6.15

* renovate

# 0.6.14

* updated models

# 0.6.13

* updated models repo
* use new `/apps/{app_id}/portal-app-preview` endpont for app data
* delete job from examination

# 0.6.12

* renovate

# 0.6.11

* changed response for cases endpoints in v2 API

# 0.6.10

* renovate

# 0.6.9

* added missing descriptions in API endpoints

# 0.6.8

* adapted for changes in MDS collection API

# 0.6.7

* adapted CSP settings for compatibility with Safari Browsers
* removed support for MPS v0 API

# 0.6.6

* renovate

# 0.6.5

* user app banner as store preview url

# 0.6.4

* added "deleted" to WBS2 case model
* added "has_frontend" to WBS1 app model
* added distinction in frontend endpoint whether app_ui_url is set or not (needed for EATS)

# 0.6.3

* renovate

# 0.6.2

* adapted for changes in MDS collection API

# 0.6.1

* renovate

# 0.6.0

* included Workbench-Daemon and using pyzmq to send update ticks to daemon

# 0.5.132

* renovate

# 0.5.131

* reverted previous change

# 0.5.130

* fix for keycloak_id

# 0.5.129

* get organization by keycloak_id

# 0.5.128

* throw 423 when examination is closed
* moved app-ui-config endpoint
* added missing examination open checks

# 0.5.127

* added `case_id` and `examination_state` to scope
* added endpoint to obtain app ui configuration
* check open examination for creation/deletion actions in v2 API

# 0.5.126

* renovate

# 0.5.125

* bugfix for manual in portal app media

# 0.5.124

* added app-ui-config to v2 frontends endpoint
* added organization header for mps v1 requests
* updated models

# 0.5.123

* renovate

# 0.5.122

* renovate

# 0.5.121

* renovate

# 0.5.120

* bugfix in store description

# 0.5.119

* adapted WBS to updated MPS models

# 0.5.118

* bug fix for missing mds/local_id mapping

# 0.5.117

* optimized fetch case endpoints

# 0.5.116

* bug fix for stain/tissue not in definitions

# 0.5.115

* renovate

# 0.5.114

* refactored slow wbs get cases / slides routes

# 0.5.113

* renovate

# 0.5.112

* fix for stop job execution route

# 0.5.111

* added error log

# 0.5.110

* renovate

# 0.5.109

* fix for non optional fields in app model

# 0.5.108

* implement stop job execution route for v1/v2
* refactored mps v2 connector

# 0.5.107

* renovate

# 0.5.106

* renovate

# 0.5.105

* fix for aggregate app v1 (`has_frontend`)

# 0.5.104

* change mps mock to v1 routes

# 0.5.103

* renovate

# 0.5.102

* fixed openapi documentation for image responses in v1 and v2, such that redoc also shows the response type correctly

# 0.5.101

* updated dependencies

# 0.5.100

* fixed class-namespace endpoint

# 0.5.99

* added secure default 'self' for app-ui frame ancestors

# 0.5.98

* added support for app-ui connect-src setting

# 0.5.97

* added support for app-ui frame ancestors

# 0.5.96

* updated receiver auth

# 0.5.95

* updated ci

# 0.5.94

* removed Solution Store Service dependencies
* added Marketplace and AAA Service dependencies

# 0.5.93

* missing await in slide regions endpoint

# 0.5.92

* updated models and added slide regions endpoint to scopes v2

# 0.5.91

* added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

# 0.5.90

* allow all origins, which is now possible because frontends do no more use client credentials (instead they explicitly
use an authorization header)

# 0.5.89

* minor fix regarding error logging when public key for scope token validation could not be retrieved from the
examination service

# 0.5.88

* added refreshing of public key for scope token validation every 300 seconds. This is required, because if the
examination service container is recreated, then RSA keys are generated and the scope token validation in the WBS
would fail because of the old cached public key.

# 0.5.87

* update models submodule for changes of AccessTokenTools class

# 0.5.86

* updated models submodule for changes of AccessTokenTools class, which creates the RSA keys no more implicitly in the
constructor.

# 0.5.85

* Create rsa keys for frontend token creation before starting the fastapi app, because otherwise there will be a race
condition due to the parallel fastapi worker processes and the worker processes might use different rsa keys.

# 0.5.84

* reverted job statuses

# 0.5.83

* removed deprecated job statuses
* added angular-specific CSP-policy

# 0.5.82

* fix for 0.5.80

# 0.5.81

* added content security policy to frontends endpoint
* refactored class validation in jobs/run endpoint
* added env variable `WBS_SCOPE_TOKEN_EXP`

# 0.5.80

* updated receiver-auth
* added setting to rewrite URL in wellknown

# 0.5.79

* added /v1/examinations/{examination_id}/apps/{app_id}/ead

# 0.5.78

* implemented /v2/frontends API

# 0.5.77

* added validation for classes of inputs
* only allow roi class creation by param `is_roi`

# 0.5.76

* added job creator types in v1

# 0.5.75

* fixed adding job to examination
* posting ead to js if non existant on scope creation
* handle parallel requests on scope creation

# 0.5.74

* fixed auth in openapi docs
* updated fastapi
* updated empaia_receiver_auth

# 0.5.73

* fix error in WBS 2 scopes data endpoints

# 0.5.72

* fixed error in position endpoint

# 0.5.71

* added jobs-out-of-scope endpoint in v2 API

# 0.5.70

* added post and delete route for collections/items

# 0.5.69

* add scoped job routes for v2 API

# 0.5.68

* removed jobs filter from user context annotation queries in v1 API

# 0.5.67

* implemented v2 data routes (annotations, classes, collections)
* added ead to scope model

# 0.5.66

* when requesting apps from the solution store the app_ui_url is set if provided

# 0.5.65

* implemented primitives routes for scope

# 0.5.64

* implemented slide routes for scope

# 0.5.63

* add GET /v2/scopes/{scope_id}/jobs route
* use router based structure for scope routes

# 0.5.62

* return http status 412 when the user-id does not match the sub in the auth token for api v1

# 0.5.61

* fixed scope id verification when validating the scope access token
* return http status 412 when the user-id or scope_id does not match the sub in the auth token or scope access token

# 0.5.60

* added v2 slide routes

# 0.5.59

* bugfix with route GET v2/examinations/id/apps/
* implemented the route /v2/scopes/{scope_id}
* added scope_validator.scope_depends() for authentication using a scope access token in the Authorization header
* validating the scope access token for route /v2/scopes/{scope_id}

# 0.5.58

* minor refactoring concerning JobToken / ScopeToken

# 0.5.57

* Add v2 examination routes

# 0.5.56

* Add v2 case routes

# 0.5.55

* Added property deleted in case and slide models

# 0.5.54

* examinations are no longer filter by `creators = [current_user_id]`

# 0.5.53

* configurable token URL path

# 0.5.52

* removed automatic client sub determination

# 0.5.51

* changed fallback store URL to <https://www.empaia.org/>

# 0.5.50

* refactoring to support API v2

# 0.5.49

* removed logging timestamps

# 0.5.48

* added endpoint to delete job input

# 0.5.47

* updated for changes in CDS models
* added tissue and stain region mapping
* updated models

# 0.5.46

* added new annotation query endpoints
* changed endpoints for unique class values
* updated models

# 0.5.45

* added `image_channels` param to tile endpoint
* adjusted thumbnail/label openapi documentation

# 0.5.44

* new CDS slide models with str for organ and stain

# 0.5.43

* integrated receiver auth class

# 0.5.42

* adjusted to new SSS app["mapp"]["description"], which now contains a dict (e.g. sub objects "en", "de")

# 0.5.41

* added models and endpoint to retrieve tree node sequence

# 0.5.40

* updated job related models
* added new endpoint `GET /v1/jobs/{job_id}/slides`
* removed * 1000 from all timestamps (no longer cobverting from milliseconds to seconds)

# 0.5.39

* logging with timestamps

# 0.5.38

* id-mapper is now optional. not setting the env variable will result in default = None and not use the mapper
* fixed timestamp bug in cases-view
* no more pydantic parsing fetching annotations

# 0.5.37

* setable chunkzize (streams) and timeout
  * WBS_CONNECTION_CHUNKSIZE
  * WBS_CONNECTION_TIMEOUT

# 0.5.36

* fix "user_id" header to JES, which should be "user-id" (no underscore)

# 0.5.35

* fixes errors due to performance changes last version

# 0.5.34

* performance:
  * using GET cases with new ?with_slides=True parameter
  * using PUT jobs/query with new jobs (id list) parameter
* requests to the JES now set "user-id" header
  * WBS_USER_SUB settings as fallback without auth

# 0.5.33

* fixed error in job wizard

# 0.5.32

* changed logic for closing examinations

# 0.5.31

* updated for ES changes

# 0.5.30

* removed settings logging

# 0.5.29

* preparations for open-source
  * remove PSQL dependency
  * moved empaia auth integration to a separate repo

# 0.5.28

* replaced dummy store_url for apps that have store_url = None

# 0.5.27

* added comment in job wizard finalize class locking

# 0.5.26

* fixed missing roi class lock in job wizard

# 0.5.25

* fixed bug introduced by previous refactoring

# 0.5.24

* set with_classes=False in classes query to improve performance
* moved classes query route to classes panel file
* refactored and simplified auth hooks
* made auth hooks async for http_client usage

# 0.5.23

* fixing bug: wrong order for put ead during wizard creation

# 0.5.22

* do not catch/hide internal server errors of job service when retrieving cases

# 0.5.21

* fixed bug, where job missing in MDS resulted in error when retrieving cases

# 0.5.20

* added explicit model for classes responses

# 0.5.19

* changed collection items models for updated Annotation Service

# 0.5.18

* changed job wizard
  * routes
  * internal logic
* changed class namespaces response
* added chack for open examination when posting new examination

# 0.5.17

* updated sender auth

# 0.5.16

* updated sender auth

# 0.5.15

* fixed global class namespaces to work when build as docker image
* changed job wizard to use new job service endpoints (post job, get token)

# 0.5.14

* proxy fix typo

# 0.5.13

* cases and slides now contain id mapping information
* included class namespaces in classes endpoints

# 0.5.12

* update empaia-sender-auth with ProxySettings

# 0.5.11

* remove trailing slash of some job-wizard MDS URLs

# 0.5.10

* debug logging
* README, ENV, CI refactoring

# 0.5.9

* thumbnail endpoint now delivers true thumbnails again.

# 0.5.8

* Hotfix to avoid WSI-Endpoint for thumbnails which causes memory leak
  * WBS delivers dummy thumbnails instead

# 0.5.7

* fixed typo in JES executions route

# 0.5.6

* set job status at MDS correctly during finalize job wizard.
* filter new apps popup by accepted apps only.

# 0.5.5

* updated sender_auth

# 0.5.4

* Changed Classes Model to allow `None`to be valid list item
* added class panel with `GET /v1/slides/{slide_id}/classes`
* the follwoing routes now set the `with_low_npp_centroids=True` param to True internally when request query includes `viewport`
  * `PUT "/v1/slides/{slide_id}/jobs/{job_id}/annotations/query`
  * `PUT "/v1/slides/{slide_id}/annotations/query`
* updated sender_auth

# 0.5.3

* `/cases` now only counts examinations created by the current user
* bugfix in `/v1/slides/{slide_id}/jobs/{job_id}/classes`

# 0.5.2

* `/cases/{id}/apps` works as intended now by fetching ALL SSS Apps
* Model property `App.store_docs_url` is now optional (not mandatory when vendors add new apps)
* the follwoing routes now set the `with_low_npp_centroids=True` param to True internally when requesting annotations from MDS
  * `PUT "/v1/slides/{slide_id}/jobs/{job_id}/annotations/query`
  * `PUT "/v1/slides/{slide_id}/annotations/query`

# 0.5.1

* set fixed order for app inputs and improved error handling
* fix for handling incomplete AAA orgnaization data (e.g. picture) and SSS app data (e.g. store_url)
* updated sender-auth submodule

# 0.5.0

* added new result tree route
* changed jobs handling in annotation queries

# 0.4.3

* catch exception for streaming response from upstream service

# 0.4.2

* update empaia-sender-auth-submodule

# 0.4.1

* add job model docs and solve JobList renaming problem

# 0.4.0

* fixed all models and endpoints to comply with latest services versions as of  2021-06-21

# 0.3.10

* use Enum instead of Literal for all occurrences of reference_type

# 0.3.9

* created jobs are now sent to the job-execution-serviuce (JES)
* user created annotations in viewer module get now ROI class assigned

# 0.3.8

* added annotation query position routes
* changed result tree routes
* updated some models and their documentation
* removed duplicate model

# 0.3.7

* added panel and routes for job creation wizard

# 0.3.6

* added Solution Store Service (SSS)

# 0.3.5

* added delete annotation, post collections and put examination state endpoint

# 0.3.4

* fixed keyword error "case_id" -> "cases" after examination models refactoring.

# 0.3.2

* fixed keyword error "count" -> "item_count" after examination models refactoring.

# 0.3.0

* updated Clinical Data Service and Job Service with breaking changes in models due to creator_type being added.

# 0.2.6

* enriched new API models and added stricter validation.

# 0.2.5

* defined own API models to make OpenAPI definition more consistent for web frontend (angular code generator)
  * removed dependencies to data service models

# 0.2.3

* added alive endpoint and test

# 0.2.2

* added centroids to PostAnnotation models

# 0.2.1

* added centroids to Annotations models

# 0.2.0

* reworked API models for more consistency (required for frontend dev)
  * now use simplified internal models
  * shared models have been removed

# 0.1.0

## 2021-03-26

* added versioning via `from importlib.metadata import version`

## 2021-03-10

* added /cases/{case_id}. returns item just like in items of /cases

## 2021-03-03

* added creators=user_id in route "/v1/cases/{case_id}/slides",
* added metadata of slides to CaseView "/v1/cases"
