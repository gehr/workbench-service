FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.3.3@sha256:1accec18ce963ac559488f9a93b6c14e65b400e771104bf8a66691d2d94b9bea AS builder
COPY . /wbs
WORKDIR /wbs
RUN poetry build && poetry export --without-hashes -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.2.17@sha256:e90ffc2866a14c5750e78d6a4bbf5500afacde366e2703e36e0f87dac16e3c20
COPY --from=builder /wbs/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /wbs/dist /artifacts
RUN pip install /artifacts/*.whl
WORKDIR /home/appuser/
COPY ./run.sh /opt/app/bin/run.sh
