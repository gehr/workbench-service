import requests

from .singletons import wbs_url


def test_alive():
    url = f"{wbs_url}/alive"
    r = requests.get(url)
    assert r.status_code == 200
    assert r.json()["status"] == "ok"
