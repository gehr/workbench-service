# Workbench-Service

## Prerequisite
1. python installed
2. poetry installed
   * see docs: https://python-poetry.org/docs/
3. docker installed


**ALL COMMANDS MUST BE RUN FROM PROJECT ROOT !**


## Setup

Create venv and install dependencies:

```bash
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

## Run in Platform-Deployment

Set environment variables in your shell or in a `.env` file:

```bash
cp env_platform_deployment_testsuite-ci .env
```

Start server:

```bash
uvicorn --host=0.0.0.0 --port=8000 workbench_service.app:app --reload
```

## Notes on ENV variables:

* `WBS_ORGANIZATION_ID`: Organization ID from Keycloak. Is sent as `organization-id` header with requests to JES.
* `WBS_ID_MAPPER_URL`: IDMS URL. Do not set if there is no ID-Mapper available.

## Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
* run tests with `pytest`

```bash
black .
isort .
pycodestyle workbench_service tests tests_auth
pylint workbench_service tests tests_auth
```

```bash
cp sample.env .env  # update WBS_CLIENT_SECRET
docker-compose up --build -d
pytest tests -rx --maxfail=1
```

## Recommended VSCode Settings

In `.vscode/settings.json`.

```json
{
    "python.linting.pylintEnabled": true,
    "python.linting.pycodestyleEnabled": false,
    "python.formatting.provider": "black"
}
```

### Usage

* http://localhost:8000/docs (only private APIs)
* http://localhost:8000/v2/docs
* http://localhost:8000/v2/scopes/docs (work in progress)
