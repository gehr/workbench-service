import asyncio
from dataclasses import dataclass
from typing import List

from .singletons import client_id_trimmed, http_client, logger, mds_url, mps_url, organization_id


@dataclass
class TriggerMatch:
    case_id: str
    slide_id: str
    trigger_id: str
    portal_app_id: str


@dataclass
class AppJob:
    app_id: str
    job_id: str
    trigger_id: str


async def setup_preprocessing_jobs():
    try:
        triggers = await _fetch_preprocessing_triggers()
        requests = await _fetch_open_preprocessing_requests()
        await _process_preprocessing_requests(requests, triggers)
    except Exception as exc:
        logger.error(exc, exc_info=True)


async def _fetch_preprocessing_triggers():
    result = await http_client.get(f"{mds_url}/v3/preprocessing-triggers")
    return result["items"]


async def _fetch_open_preprocessing_requests():
    payload = {"states": ["OPEN"]}
    result = await http_client.put(f"{mds_url}/v3/preprocessing-requests/query", json=payload)
    return result["items"]


async def _process_preprocessing_requests(requests, triggers):
    tasks = []
    for request in requests:
        try:
            tasks.append(_process_preprocessing_request(request, triggers))
        except Exception as exc:
            logger.error(exc, exc_info=True)
    await asyncio.gather(*tasks)


async def _process_preprocessing_request(request, triggers):
    slide = await _fetch_slide(request["slide_id"])
    matches = _match_triggers_on_slide(triggers, slide)
    app_jobs = await _setup_jobs_for_trigger_matches(request, matches)
    await _close_preprocessing_request(request["id"], app_jobs)


async def _fetch_slide(slide_id):
    return await http_client.get(f"{mds_url}/v3/slides/{slide_id}")


async def _setup_jobs_for_trigger_matches(request, matches: List[TriggerMatch]) -> List[AppJob]:
    tasks = [_setup_job_for_trigger_match(match) for match in matches]
    return await asyncio.gather(*tasks)


async def _setup_job_for_trigger_match(match: TriggerMatch) -> AppJob:
    job = await _setup_ready_preprocessing_job(match.portal_app_id, match.slide_id)
    examination = await _get_or_create_open_examination(match.case_id, job["app_id"])
    await _add_job_to_examination(job["id"], examination["id"])
    return AppJob(app_id=job["app_id"], job_id=job["id"], trigger_id=match.trigger_id)


async def _setup_ready_preprocessing_job(portal_app_id, slide_id):
    active_app = await _fetch_active_app(portal_app_id)
    input_key = _get_preprocessing_input_key(active_app["ead"])
    job = await _post_preprocessing_job(active_app["id"])
    await _update_job_input(job["id"], input_key, slide_id)
    await _update_job_status_to_ready(job["id"])
    return job


async def _get_or_create_open_examination(case_id, app_id):
    payload = {"case_id": case_id, "creator_id": client_id_trimmed, "creator_type": "SERVICE", "app_id": app_id}
    return await http_client.put(f"{mds_url}/v3/examinations", json=payload)


async def _add_job_to_examination(job_id, examination_id):
    await http_client.put(f"{mds_url}/v3/examinations/{examination_id}/jobs/{job_id}/add")


async def _fetch_active_app(portal_app_id):  # think about LRU cache decoration
    headers = {"organization-id": organization_id}
    portal_app = await http_client.get(f"{mps_url}/v1/customer/portal-apps/{portal_app_id}", headers=headers)
    app_view = portal_app["active_app_views"]["v3"]["app"]
    return app_view


async def _post_preprocessing_job(app_id):
    payload = {
        "app_id": app_id,
        "mode": "PREPROCESSING",
        "creator_id": client_id_trimmed,
        "creator_type": "SERVICE",
    }
    return await http_client.post(f"{mds_url}/v3/jobs", json=payload)


async def _update_job_input(job_id, input_key, slide_id):
    payload = {"id": slide_id}
    await http_client.put(f"{mds_url}/v3/jobs/{job_id}/inputs/{input_key}", json=payload)


async def _update_job_status_to_ready(job_id):
    payload = {"status": "READY"}
    await http_client.put(f"{mds_url}/v3/jobs/{job_id}/status", json=payload)


async def _close_preprocessing_request(request_id, app_jobs: List[AppJob]):
    payload = {"app_jobs": [{"app_id": aj.app_id, "job_id": aj.job_id, "trigger_id": aj.trigger_id} for aj in app_jobs]}
    await http_client.put(f"{mds_url}/v3/preprocessing-requests/{request_id}/process", json=payload)


def _match_triggers_on_slide(triggers, slide) -> List[TriggerMatch]:
    trigger_matches = []
    for trigger in triggers:
        if _trigger_matches_slide(trigger, slide):
            trigger_matches.append(
                TriggerMatch(
                    case_id=slide["case_id"],
                    slide_id=slide["id"],
                    trigger_id=trigger["id"],
                    portal_app_id=trigger["portal_app_id"],
                )
            )
    return trigger_matches


def _trigger_matches_slide(trigger, slide) -> bool:
    return trigger["stain"] == slide["stain"] and trigger["tissue"] == slide["tissue"]


def _get_preprocessing_input_key(ead):
    assert "preprocessing" in ead["modes"]
    return ead["modes"]["preprocessing"]["inputs"][0]
