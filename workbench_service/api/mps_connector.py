from typing import Callable

from fastapi import HTTPException

from workbench_service.api.aaa_connector import fetch_raw_organizations

from ..singletons import ead_cache, http_client, mps_url, settings


async def parse_store_description(store_description: dict) -> str:
    translated_description = store_description
    # refactor when client offers different languages
    if isinstance(store_description, list):
        translated_description = await get_translation(store_description, "EN")
        if translated_description is None:
            translated_description = await get_translation(store_description, "DE")
    return translated_description


async def get_translation(translation_list: list, language_abbr: str) -> str:
    for translation in translation_list:
        if str(translation["lang"]).upper() == language_abbr.upper():
            return translation["text"]
    return None


async def fetch_app_view(app_id: str, api_version: str) -> dict:
    raw_app_views = await query_app_views({"apps": [app_id]}, api_version)
    if len(raw_app_views) == 0:
        raise HTTPException(404, f"Could not find app with id {app_id}")
    return raw_app_views[0]


async def query_app_views(query: dict, api_version: str) -> list:
    if not query.get("apps"):
        return []
    if len(query.get("apps")) == 0:
        return []
    query["api_versions"] = [api_version]
    headers = {"organization-id": settings.organization_id}
    raw_app_views = await http_client.put(
        f"{settings.marketplace_service_url}/v1/customer/app-views/query", json=query, headers=headers
    )
    return raw_app_views


async def get_app_ui_url(app_id: str, api_version: str) -> str:
    raw_app_view = await fetch_app_view(app_id, api_version)
    app_ui_url = raw_app_view.get("app").get("app_ui_url")
    has_frontend = bool(raw_app_view.get("app").get("has_frontend"))
    if has_frontend:
        if app_ui_url:
            return app_ui_url
        else:
            return f"{settings.marketplace_service_url}/v1/customer/apps/{app_id}/app-ui-files"
    elif api_version == "v2" and settings.generic_app_ui_v2_url:
        return settings.generic_app_ui_v2_url
    elif api_version == "v3" and settings.generic_app_ui_v3_url:
        return settings.generic_app_ui_v3_url
    return None


async def fetch_raw_app_views(query: dict, api_version: str) -> dict:
    headers = {"organization-id": settings.organization_id}
    raw_portal_apps = await http_client.put(f"{mps_url}/v1/customer/portal-apps/query", json=query, headers=headers)

    raw_app_views_vx = []
    for raw_p_app in raw_portal_apps["items"]:
        if api_version in raw_p_app["active_app_views"]:
            raw_app_view = raw_p_app["active_app_views"][api_version]
            if raw_app_view:
                raw_app_views_vx.append(raw_app_view)

    return raw_app_views_vx


async def fetch_raw_ead(app_id: str, api_version: str) -> dict:
    ead = ead_cache.get(app_id)
    if not ead:
        raw_app_view = await fetch_app_view(app_id, api_version)
        ead = await get_ead(raw_app_view, app_id)
        ead_cache.add(app_id, ead)
    return ead


async def get_ead(raw_app: dict, app_id: str) -> dict:
    if raw_app["app"]["ead"] is None:
        raise HTTPException(404, f"Could not find ead for app with id {app_id}")
    return raw_app["app"]["ead"]


async def get_app_ui_config(app_id: str) -> dict:
    app_ui_config_url = f"{settings.marketplace_service_url}/v1/customer/apps/{app_id}/app-ui-config"
    app_ui_config = None
    try:
        headers = {"organization-id": settings.organization_id}
        app_ui_config = await http_client.get(app_ui_config_url, headers=headers)
    except HTTPException as e:
        if e.status_code == 404:
            pass
        else:
            raise e
    return app_ui_config


async def fetch_raw_tags() -> dict:
    url = f"{mps_url}/v1/public/tags"
    raw_tags = await http_client.get(url)

    # delete non relevant tags from dict
    try:
        del raw_tags["image_types"]
        del raw_tags["procedures"]
        del raw_tags["scanners"]
    except KeyError:
        pass

    return raw_tags


async def fetch_raw_app_views_approved(query: dict, api_version: str) -> list:
    headers = {"organization-id": settings.organization_id}
    raw_portal_apps = await http_client.put(f"{mps_url}/v1/customer/portal-apps/query", json=query, headers=headers)

    items = []
    for raw_p_app in raw_portal_apps["items"]:
        if not raw_p_app or raw_p_app["status"] != "LISTED":
            continue

        if api_version in raw_p_app["active_app_views"]:
            raw_app_view = raw_p_app["active_app_views"][api_version]
            if not raw_app_view or raw_app_view["status"] != "APPROVED":
                continue

            raw_app = raw_app_view["app"]
            if not raw_app or raw_app["status"] != "APPROVED":
                continue

            if await get_app_ui_url(raw_app["id"], api_version):
                items.append(raw_app_view)

    return items


async def fetch_raw_aggregated_apps(
    query: dict, job_modes: list, api_version: str, aggregate_app_func: Callable
) -> list:
    items = []
    raw_app_views = await fetch_raw_app_views(query, api_version)
    raw_organizations = await fetch_raw_organizations()
    for a in raw_app_views:
        if job_modes is not None:
            modes = [m.lower() for m in job_modes]
            if not set(modes).issubset(set(a["app"]["ead"]["modes"].keys())):
                continue
        app = await aggregate_app_func(raw_app_view=a, raw_organizations=raw_organizations)
        items.append(app)
    return {
        "items": items,
        "item_count": len(items),
    }


async def fetch_raw_aggregated_app_by_portal_app_id(
    portal_app_id: str, api_version: str, aggregate_app_func: Callable
) -> dict:
    raw_organizations = await fetch_raw_organizations()
    headers = {"organization-id": settings.organization_id}
    raw_portal_app = await http_client.get(f"{mps_url}/v1/customer/portal-apps/{portal_app_id}", headers=headers)
    raw_app_view = raw_portal_app["active_app_views"][api_version]
    if raw_app_view:
        return await aggregate_app_func(raw_app_view=raw_app_view, raw_organizations=raw_organizations)
    return None
