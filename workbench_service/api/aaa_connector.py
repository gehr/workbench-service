from ..singletons import aaa_url, http_client


async def fetch_raw_organization_by_uuid(keycloak_id: str) -> dict:
    raw_organization = await http_client.get(f"{aaa_url}/api/v2/public/organizations/{keycloak_id}")
    return raw_organization


async def fetch_raw_organizations() -> dict:
    params = {"page_size": 9999}
    raw_organizations = await http_client.get(f"{aaa_url}/api/v2/public/organizations", params=params)
    orga_dict = {}
    for organization in raw_organizations["organizations"]:
        orga_dict[organization.get("organization_id")] = organization
    return orga_dict


def get_organization_logo_url(raw_organization: dict) -> str:
    fallback_img_url = "https://upload.wikimedia.org/wikipedia/commons/6/6d/Windows_Settings_app_icon.png"
    img = raw_organization.get("logo_url", fallback_img_url)
    return img


def get_organization_name(raw_organization: dict) -> str:
    fallback_name = "FALLBACK_VENDOR_NAME"
    name = raw_organization.get("organization_name", fallback_name)
    return name
