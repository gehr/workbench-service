from fastapi import Depends

from .validation_helpers import validate_token


class FrontendTokenValidator:
    _verification_key: str = None

    def __init__(self, access_token_tools):
        self.access_token_tools = access_token_tools

    async def __call__(self, frontend_token: str):
        return await self._validate_token(frontend_token)

    async def _get_verification_key(self) -> bytes:
        if self._verification_key is None:
            key = self.access_token_tools.public_key.decode("ascii")
            self._verification_key = key
        return self._verification_key

    async def _validate_token(self, access_token: str):
        key = await self._get_verification_key()
        return await validate_token(access_token, key)


class FrontendValidation:
    def __init__(self, access_token_tools):
        self.frontend_token_validator = FrontendTokenValidator(access_token_tools)

    def frontend_depends(self):
        return Depends(self.frontend_token_validator)
