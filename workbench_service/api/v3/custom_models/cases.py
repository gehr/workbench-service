from enum import Enum
from typing import List, Optional

from pydantic import AnyHttpUrl, Field

from ....models.v3.clinical import CaseCreatorType
from ....models.v3.commons import Id, ItemCount, RestrictedBaseModel, Timestamp
from ....models.v3.examination import Examination as SimpleExamination


class PreprocessingProgress(Enum):
    NONE = "none"
    RUNNING = "running"
    FINISHED = "finished"


class Case(RestrictedBaseModel):
    id: Id = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="ID")
    mds_url: Optional[AnyHttpUrl] = Field(
        example="https://mds.example.org",
        description="Base URL of Medical Data Service instance that generated empaia_id",
    )
    local_id: Optional[Id] = Field(
        example="3a1e3715-8945-4c13-8fc6-9c1c549cf85d", description="Local ID provided by AP-LIS"
    )
    creator_id: str = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="Creator ID")
    creator_type: CaseCreatorType = Field(example=CaseCreatorType.USER, description="Creator type")
    description: Optional[str] = Field(example="Some text", description="Case description")
    created_at: Timestamp = Field(
        example=1598611645000, description="Timestamp (milliseconds) when the case was created"
    )
    updated_at: Timestamp = Field(
        example=1598611645000, description="Timestamp (milliseconds) when the case was last updated"
    )
    slides_count: ItemCount = Field(example=10, description="Number of slides in case")
    tissues: dict = Field(
        example={
            "SKIN": {"EN": "Skin", "DE": "Haut"},
            "SOFT_TISSUE": {"EN": "Soft tissue", "DE": "Weichteilgewebe"},
            "BONE_AND_CARTILAGE": {"EN": "Bone and cartilage", "DE": "Knochen und Knorpel"},
        },
        description="List of tissue of all slides in case",
    )
    blocks: List[str] = Field(example=["A1", "A2"], description="List of all blocks of slides in case")
    stains: dict = Field(
        example={
            "H_AND_E": {"EN": "H&E", "DE": "H&E"},
            "HER2": {"EN": "HER2", "DE": "HER2"},
            "KI67": {"EN": "Ki67", "DE": "Ki67"},
        },
        description="List of all stains of all slides in case",
    )
    preprocessing_progress: PreprocessingProgress = Field(
        example=PreprocessingProgress.NONE, description="Progress of the preprocessing jobs running within the case"
    )
    examinations: List[SimpleExamination] = Field(description="Examinations in case")
    deleted: Optional[bool] = Field(
        example=False,
        description="Flag indicating whether the case and all underlying slide files and mappings have been deleted",
    )


class CaseList(RestrictedBaseModel):
    items: List[Case] = Field(
        description="List of cases accessible by user (list content is affected by skip/limit pagination)"
    )
    item_count: ItemCount = Field(
        example=20,
        description="Total number of cases accessible by user (number is not affected by skip/limit pagination)",
    )
