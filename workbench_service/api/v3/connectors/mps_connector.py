from typing import List

from workbench_service.models.v3.job import JobMode

from ....singletons import settings
from ... import aaa_connector
from ... import mps_connector as generic_mps_connector
from ..custom_models.apps import App, AppList

###################################
# Just call generic_mps_connector #
# with appropriate version        #
###################################


async def parse_store_description(store_description: dict) -> str:
    return await generic_mps_connector.parse_store_description(store_description)


async def get_translation(translation_list: list, language_abbr: str) -> str:
    return await generic_mps_connector.get_translation(translation_list, language_abbr)


async def fetch_app_view(app_id: str) -> dict:
    return await generic_mps_connector.fetch_app_view(app_id, "v3")


async def query_app_views(query: dict) -> list:
    return await generic_mps_connector.query_app_views(query, "v3")


async def get_app_ui_url(app_id: str) -> str:
    return await generic_mps_connector.get_app_ui_url(app_id, "v3")


async def fetch_raw_app_views(query: dict) -> dict:
    return await generic_mps_connector.fetch_raw_app_views(query, "v3")


async def fetch_raw_ead(app_id: str) -> dict:
    return await generic_mps_connector.fetch_raw_ead(app_id, "v3")


async def get_ead(raw_app: dict, app_id: str) -> dict:
    return await generic_mps_connector.get_ead(raw_app, app_id)


async def get_app_ui_config(app_id: str) -> dict:
    return await generic_mps_connector.get_app_ui_config(app_id)


async def fetch_raw_tags() -> dict:
    return await generic_mps_connector.fetch_raw_tags()


async def fetch_raw_app_views_approved(query: dict) -> list:
    return await generic_mps_connector.fetch_raw_app_views_approved(query, "v3")


async def fetch_raw_aggregated_app_by_portal_app_id(portal_app_id: str) -> dict:
    return await generic_mps_connector.fetch_raw_aggregated_app_by_portal_app_id(
        portal_app_id=portal_app_id,
        api_version="v3",
        aggregate_app_func=aggregate_app_view,
    )


async def fetch_raw_aggregated_apps(query: dict, job_modes: List[JobMode]) -> dict:
    return await generic_mps_connector.fetch_raw_aggregated_apps(
        query=query,
        job_modes=job_modes,
        api_version="v3",
        aggregate_app_func=aggregate_app_view,
    )


####################################
# Parse to appropriate WBS version #
####################################


async def fetch_aggregated_apps(query: dict, job_modes: List[JobMode]) -> AppList:
    raw_aggr_apps = await fetch_raw_aggregated_apps(query, job_modes)
    return AppList.model_validate(raw_aggr_apps)


async def fetch_aggregated_app_by_app_id(app_id: str) -> App:
    raw_organizations = await aaa_connector.fetch_raw_organizations()
    raw_app_view = await fetch_app_view(app_id)
    raw_aggr_app = await aggregate_app_view(raw_app_view=raw_app_view, raw_organizations=raw_organizations)
    return App.model_validate(raw_aggr_app)


async def fetch_aggregated_app_by_portal_app_id(portal_app_id: str) -> App:
    raw_aggr_app = await fetch_raw_aggregated_app_by_portal_app_id(portal_app_id)
    return App.model_validate(raw_aggr_app)


async def aggregate_app_view(raw_app_view: dict, raw_organizations: dict) -> dict:
    organization_id = raw_app_view["organization_id"]
    raw_organization = raw_organizations[organization_id]
    organization_name = aaa_connector.get_organization_name(raw_organization)
    organization_logo_url = aaa_connector.get_organization_logo_url(raw_organization)

    store_description = await parse_store_description(raw_app_view["details"]["description"])

    store_docs_url = None
    store_preview_before_url = None
    store_preview_after_url = None
    media = raw_app_view["media"]
    if media:
        if "manual" in media and len(media["manual"]) > 0:
            store_docs_url = media["manual"][0]["presigned_media_url"]
        if "banner" in media and len(media["banner"]) > 0:
            store_preview_before_url = media["banner"][0]["presigned_media_url"]
        if "banner" in media and len(media["banner"]) > 1:
            store_preview_after_url = media["banner"][1]["presigned_media_url"]

    has_frontend = False
    if raw_app_view["app"]["has_frontend"] or settings.generic_app_ui_v3_url:
        has_frontend = True

    aggregated_app = {
        "app_id": raw_app_view["app"]["id"],
        "app_version": raw_app_view["version"],
        "name_short": raw_app_view["app"]["ead"]["name_short"],
        "vendor_name": organization_name,
        "store_description": store_description,
        "store_icon_url": organization_logo_url,
        "store_url": raw_app_view["details"]["marketplace_url"],
        "store_docs_url": store_docs_url,
        "store_preview_before_url": store_preview_before_url,
        "store_preview_after_url": store_preview_after_url,
        "has_frontend": has_frontend,
        "research_only": bool(raw_app_view["research_only"]),
        "tags": raw_app_view.get("tags"),
        "portal_app_id": raw_app_view["portal_app_id"],
    }
    return aggregated_app
