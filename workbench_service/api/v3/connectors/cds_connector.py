from typing import List, Optional, Tuple

from ....singletons import http_client, mds_url
from ..custom_models.cases import Case, CaseList, PreprocessingProgress
from ..custom_models.id_mapper import MappingType
from ..custom_models.slides import Slide, SlideList
from . import es_connector, idms_connector, mps_connector


async def fetch_raw_tags() -> dict:
    raw_mps_tags = await mps_connector.fetch_raw_tags()
    cds_tags = {"STAIN": {}, "TISSUE": {}}
    for t in raw_mps_tags["tissues"]:
        cds_tags["TISSUE"][t["name"]] = {}
        for translation in t["tag_translations"]:
            cds_tags["TISSUE"][t["name"]][translation["lang"]] = translation["text"]
    for t in raw_mps_tags["stains"]:
        cds_tags["STAIN"][t["name"]] = {}
        for translation in t["tag_translations"]:
            cds_tags["STAIN"][t["name"]][translation["lang"]] = translation["text"]
    return cds_tags


async def aggregate_case(
    raw_case: dict,
    raw_tags: dict,
    case_id_mappings: dict,
    raw_examinations: dict,
    preprocessing_progress: PreprocessingProgress,
) -> Case:
    mds_url_mapping = None
    local_id_mapping = None
    if raw_case["id"] in case_id_mappings:
        mds_url_mapping = (
            case_id_mappings[raw_case["id"]]["mds_url"] if "mds_url" in case_id_mappings[raw_case["id"]] else None
        )
        local_id_mapping = (
            case_id_mappings[raw_case["id"]]["local_id"] if "local_id" in case_id_mappings[raw_case["id"]] else None
        )

    stain_tags = set([s["stain"] for s in raw_case["slides"]])
    stains = await get_tag_for_tag_group(raw_tags, stain_tags, "STAIN")

    tissue_tags = set([s["tissue"] for s in raw_case["slides"]])
    tissues = await get_tag_for_tag_group(raw_tags, tissue_tags, "TISSUE")

    blocks = set([s["block"] for s in raw_case["slides"] if s["block"] is not None])

    updated_at = raw_case["updated_at"]
    if updated_at is None:
        updated_at = raw_case["created_at"]

    return Case(
        id=raw_case["id"],
        mds_url=mds_url_mapping,
        local_id=local_id_mapping,
        creator_id=raw_case["creator_id"],
        creator_type=raw_case["creator_type"],
        description=raw_case["description"],
        created_at=raw_case["created_at"],
        updated_at=updated_at,
        slides_count=len(raw_case["slides"]),
        tissues=tissues,
        blocks=blocks,
        stains=stains,
        preprocessing_progress=preprocessing_progress,
        examinations=raw_examinations["items"],
        deleted=raw_case["deleted"],
    )


async def get_tag_for_tag_group(raw_tags: dict, group_tags: list, group: str) -> dict:
    tags = {}
    if group not in raw_tags:
        return tags
    for tag in group_tags:
        if tag and tag in raw_tags[group]:
            tags[tag] = raw_tags[group][tag]
    return tags


async def get_raw_stain_and_tissue_mapping(slide: Slide, tags: dict) -> Tuple[dict, dict]:
    tag_stain, tag_tissue = slide.get("stain"), slide.get("tissue")
    stain, tissue = None, None
    if tag_stain and tag_stain in tags["STAIN"]:
        stain = {"tag": tag_stain, "mappings": tags["STAIN"][tag_stain]}
    if tag_tissue and tag_tissue in tags["TISSUE"]:
        tissue = {"tag": tag_tissue, "mappings": tags["TISSUE"][tag_tissue]}
    return stain, tissue


async def fetch_raw_slide(slide_id: str) -> dict:
    url = f"{mds_url}/v3/slides/{slide_id}"
    raw_slide = await http_client.get(url)
    return raw_slide


async def fetch_raw_slides(cases: List[str], skip: int = None, limit: int = None) -> dict:
    url = f"{mds_url}/v3/slides/query"
    raw_query = {}
    if cases:
        raw_query["cases"] = cases
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    raw_slides = await http_client.put(url, json=raw_query, params=params)
    return raw_slides


async def fetch_raw_case(case_id: str, with_slides: bool = True) -> dict:
    url = f"{mds_url}/v3/cases/{case_id}"
    params = {"with_slides": with_slides}
    raw_case = await http_client.get(url, params=params)
    return raw_case


async def fetch_case(case_id: str, with_slides: bool = True) -> Case:
    raw_case = await fetch_raw_case(case_id, with_slides)
    raw_tags = await fetch_raw_tags()
    raw_examinations = await es_connector.fetch_raw_examinations([raw_case["id"]])
    mappings = await idms_connector.fetch_mappings([raw_case["id"]], MappingType.CASES)
    preprocessing_progress = await es_connector.get_preprocessing_progress(raw_examinations=raw_examinations)
    case = await aggregate_case(
        raw_case=raw_case,
        raw_tags=raw_tags,
        case_id_mappings=mappings,
        raw_examinations=raw_examinations,
        preprocessing_progress=preprocessing_progress,
    )
    return case


async def fetch_raw_cases(
    cases_filter: Optional[List[str]], with_slides: bool = True, skip: int = None, limit: int = None
) -> dict:
    # Imaginary endpoint
    params = {"with_slides": with_slides}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit

    if cases_filter is None:
        url = f"{mds_url}/v3/cases"
        raw_cases = await http_client.get(url, params=params)
        return raw_cases

    if len(cases_filter) == 0:
        raw_cases = {"items": [], "item_count": 0}
        return raw_cases

    url = f"{mds_url}/v3/cases/query"
    query = {"cases": cases_filter}
    raw_cases = await http_client.put(url, params=params, json=query)
    return raw_cases


async def fetch_cases(
    cases_filter: List[str], with_slides: bool = True, skip: int = None, limit: int = None
) -> CaseList:
    raw_cases = await fetch_raw_cases(cases_filter=cases_filter, with_slides=with_slides, skip=skip, limit=limit)
    return await _compile_cases(raw_cases)


async def _compile_cases(raw_cases: dict) -> CaseList:
    raw_tags = await fetch_raw_tags()

    case_ids = [raw_case["id"] for raw_case in raw_cases["items"]]
    mappings = await idms_connector.fetch_mappings(case_ids, MappingType.CASES)

    # fetch all cases
    raw_cases_dict = {}
    for raw_case in raw_cases["items"]:
        raw_case["raw_examinations"] = {"items": []}
        raw_cases_dict[raw_case["id"]] = raw_case

    raw_examinations = await es_connector.fetch_raw_examinations(list(raw_cases_dict.keys()))

    cases_jobs_dict = {}
    for raw_examination in raw_examinations["items"]:
        case_id = raw_examination["case_id"]
        raw_cases_dict[case_id]["raw_examinations"]["items"].append(raw_examination)

        if case_id not in cases_jobs_dict:
            cases_jobs_dict[case_id] = []
        cases_jobs_dict[case_id].extend(raw_examination["jobs"])

    preprocessing_dict = await es_connector.get_preprocessing_progress_dict(cases_jobs_dict)

    cases_dict = {}
    for case_id, raw_case in raw_cases_dict.items():
        preprocessing_progress = (
            preprocessing_dict[case_id] if case_id in preprocessing_dict else PreprocessingProgress.NONE
        )
        case = await aggregate_case(
            raw_case=raw_case,
            raw_tags=raw_tags,
            case_id_mappings=mappings,
            raw_examinations=raw_cases_dict[case_id]["raw_examinations"],
            preprocessing_progress=preprocessing_progress,
        )
        cases_dict[case_id] = case

    cases_list = list(cases_dict.values())
    cases = CaseList(item_count=raw_cases["item_count"], items=cases_list)
    return cases


async def fetch_slides(case_id: str, skip: int = None, limit: int = None) -> SlideList:
    raw_tags = await fetch_raw_tags()
    raw_slides = await fetch_raw_slides(cases=[case_id], skip=skip, limit=limit)
    slide_ids = [raw_slide["id"] for raw_slide in raw_slides["items"]]
    mappings = await idms_connector.fetch_mappings(slide_ids, MappingType.SLIDES)

    items = []
    for raw_slide in raw_slides["items"]:
        mds_url_mapping = None
        local_id_mapping = None
        if raw_slide["id"] in mappings:
            mds_url_mapping = mappings[raw_slide["id"]]["mds_url"] if "mds_url" in mappings[raw_slide["id"]] else None
            local_id_mapping = (
                mappings[raw_slide["id"]]["local_id"] if "local_id" in mappings[raw_slide["id"]] else None
            )

        created_at = raw_slide["created_at"]
        updated_at = raw_slide.get("updated_at")

        if updated_at is None:
            updated_at = created_at

        stain, tissue = await get_raw_stain_and_tissue_mapping(raw_slide, raw_tags)

        items.append(
            Slide(
                id=raw_slide["id"],
                mds_url=mds_url_mapping,
                local_id=local_id_mapping,
                block=raw_slide.get("block"),
                stain=stain,
                tissue=tissue,
                case_id=raw_slide["case_id"],
                created_at=created_at,
                updated_at=updated_at,
                deleted=raw_slide["deleted"],
            )
        )

    return SlideList(item_count=raw_slides["item_count"], items=items)
