from . import base, data, jobs, slides, storage


def add_routes(app, late_init):
    base.add_routes(app, late_init)
    jobs.add_routes(app, late_init)
    slides.add_routes(app, late_init)
    data.add_routes(app, late_init)
    storage.add_routes(app, late_init)
