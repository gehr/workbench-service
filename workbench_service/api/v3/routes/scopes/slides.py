from typing import List

from fastapi import Path
from fastapi.responses import StreamingResponse

from .....models.v3.commons import Id
from .....models.v3.slide import SlideInfo
from ... import params
from ...connectors import cds_connector, es_connector, wsis_connector
from ...custom_models.slides import SlideList
from ...singletons import api_integration, scope_validation

ImageResponses = {
    200: {
        "content": {
            "image/*": {"schema": {"type": "string", "format": "binary"}},
        },
        "description": "The image response. Format (e.g. png, jpeg) according to used query parameter [image_format]",
    },
}


def add_routes(app, late_init):
    @app.get(
        "/{scope_id}/slides",
        tags=["slides"],
        summary="Get slides of current scope",
        status_code=200,
        responses={200: {"model": SlideList}},
    )
    async def _(
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        scope_data = await es_connector.fetch_raw_scope_data(scope_id)
        case_id = await es_connector.fetch_case_id_for_scope(scope_data)
        return await cds_connector.fetch_slides(case_id)

    @app.get(
        "/{scope_id}/slides/{slide_id}/info",
        tags=["slides"],
        summary="Get slide info",
        status_code=200,
        response_model=SlideInfo,
    )
    async def _(
        scope_id: Id = Path(...),
        slide_id: str = params.slide_id,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await wsis_connector.validate_scope_slide(scope_id, slide_id)
        return await wsis_connector.fetch_slide_info(slide_id)

    @app.get(
        "/{scope_id}/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}",
        tags=["slides"],
        summary="Get slide tile",
        responses=ImageResponses,
        response_class=StreamingResponse,
    )
    async def _(
        scope_id: Id = Path(...),
        slide_id: str = params.slide_id,
        level: int = params.level,
        tile_x: int = params.tile_x,
        tile_y: int = params.tile_y,
        image_format: str = params.image_format,
        image_quality: int = params.image_quality,
        image_channels: List[int] = params.image_channels,
        padding_color: str = params.padding_color,
        z: int = params.z,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await wsis_connector.fetch_raw_stream_slide_tile(
            slide_id,
            level,
            tile_x,
            tile_y,
            image_format,
            image_quality,
            image_channels,
            padding_color,
            z,
        )

    @app.get(
        "/{scope_id}/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
        tags=["slides"],
        summary="Get slide region",
        responses=ImageResponses,
        response_class=StreamingResponse,
    )
    async def _(
        scope_id: Id = Path(...),
        slide_id: str = params.slide_id,
        level: int = params.level,
        start_x: int = params.start_x,
        start_y: int = params.start_y,
        size_x: int = params.size_x,
        size_y: int = params.size_y,
        image_format: str = params.image_format,
        image_quality: int = params.image_quality,
        image_channels: List[int] = params.image_channels,
        z: int = params.z,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await wsis_connector.fetch_raw_stream_slide_region(
            slide_id,
            level,
            start_x,
            start_y,
            size_x,
            size_y,
            image_format,
            image_quality,
            image_channels,
            z,
        )

    @app.get(
        "/{scope_id}/slides/{slide_id}/macro/max_size/{max_x}/{max_y}",
        tags=["slides"],
        summary="Get slide macro image",
        responses=ImageResponses,
        response_class=StreamingResponse,
    )
    async def _(
        scope_id: Id = Path(...),
        slide_id: str = params.slide_id,
        max_x: int = params.max_x,
        max_y: int = params.max_y,
        image_format: str = params.image_format,
        image_quality: int = params.image_quality,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await wsis_connector.validate_scope_slide(scope_id, slide_id)
        return await wsis_connector.fetch_raw_stream_slide_macro(slide_id, max_x, max_y, image_format, image_quality)

    @app.get(
        "/{scope_id}/slides/{slide_id}/label/max_size/{max_x}/{max_y}",
        tags=["slides"],
        summary="Get slide label image",
        responses=ImageResponses,
        response_class=StreamingResponse,
    )
    async def _(
        scope_id: Id = Path(...),
        slide_id: str = params.slide_id,
        max_x: int = params.max_x,
        max_y: int = params.max_y,
        image_format: str = params.image_format,
        image_quality: int = params.image_quality,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await wsis_connector.validate_scope_slide(scope_id, slide_id)
        return await wsis_connector.fetch_raw_stream_slide_label(slide_id, max_x, max_y, image_format, image_quality)

    @app.get(
        "/{scope_id}/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}",
        tags=["slides"],
        summary="Get slide thumbnail",
        responses=ImageResponses,
        response_class=StreamingResponse,
    )
    async def _(
        scope_id: Id = Path(...),
        slide_id: str = params.slide_id,
        max_x: int = params.max_x,
        max_y: int = params.max_y,
        image_format: str = params.image_format,
        image_quality: int = params.image_quality,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await wsis_connector.validate_scope_slide(scope_id, slide_id)
        return await wsis_connector.fetch_raw_stream_slide_thumbnail(
            slide_id, max_x, max_y, image_format, image_quality
        )
