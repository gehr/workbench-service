from .....models.v3.commons import Id, Message
from .....models.v3.examination import ScopeList
from ...connectors import es_connector, mps_connector
from ...custom_models.examinations import ExtendedScope
from ...singletons import api_integration, scope_validation, settings


def add_routes(app, late_init):
    @app.get(
        "/{scope_id}",
        tags=["scope"],
        summary="Get scope metadata",
        responses={
            200: {"model": ExtendedScope},
            404: {
                "model": Message,
                "description": "The scope was not found",
            },
        },
    )
    async def _(
        scope_id: Id,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        scope_data = await es_connector.fetch_raw_scope_data(scope_id)

        raw_examination = await es_connector.fetch_raw_examination(scope_data["examination_id"])
        raw_ead = await mps_connector.fetch_raw_ead(raw_examination["app_id"])
        scope_data["case_id"] = raw_examination["case_id"]
        scope_data["examination_state"] = raw_examination["state"]
        scope_data["ead"] = raw_ead

        return scope_data

    @app.get(
        "/{scope_id}/scopes",
        tags=["scope"],
        summary="Get all scopes of examination. If WBS_DISABLE_MULTI_USER is True, only current scope is returned.",
        responses={
            200: {"model": ScopeList},
            500: {"model": Message, "description": "No scopes could be retrieved"},
        },
    )
    async def _(
        scope_id: Id,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        scope_data = await es_connector.fetch_raw_scope_data(scope_id)

        if settings.disable_multi_user:
            return {"item_count": 1, "items": [scope_data]}
        else:
            return await es_connector.query_scopes(scope_data["examination_id"])
