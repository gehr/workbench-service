from http import HTTPStatus

from fastapi import Header, HTTPException, Response
from fastapi.responses import JSONResponse

from workbench_service.api.v3.connectors import cds_connector, jes_connector
from workbench_service.models.marketplace.app import AppUiConfiguration
from workbench_service.models.v3.commons import Message
from workbench_service.models.v3.examination import ExaminationQuery, ExaminationState

from .. import params
from ..connectors import es_connector, mps_connector
from ..custom_models.apps import FrontendToken
from ..custom_models.examinations import Examination, ExaminationList, PostExamination, ScopeTokenAndScopeID
from ..custom_models.jobs import JOB_FINISHED_STATES
from ..singletons import access_token_tools, api_integration, settings
from .scopes.jobs import finalize_job


def add_routes(app, late_init):
    @app.get(
        "/examinations/{examination_id}",
        tags=["examinations"],
        status_code=200,
        responses={200: {"model": Examination}},
    )
    async def _(
        examination_id: str = params.ex_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        examination = await es_connector.fetch_examination(examination_id=examination_id)
        return examination

    @app.put(
        "/examinations/{examination_id}/close",
        tags=["examinations"],
        status_code=200,
        responses={200: {"model": Examination}},
    )
    async def _(
        examination_id: str = params.ex_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        examination = await es_connector.fetch_examination(examination_id)

        if examination.state == "CLOSED":
            return examination

        case_id = None
        if settings.enable_annot_case_data_partitioning:
            case_id = examination.case_id

        jobs = examination.jobs
        for job in jobs:
            job = job.model_dump()
            if job["containerized"]:
                if job["status"] in JOB_FINISHED_STATES:
                    continue
                else:
                    await finalize_job(job, "ERROR", "Examination got closed", case_id=case_id)
                    try:
                        await jes_connector.stop_job_execution(job["id"])
                    except HTTPException:
                        pass
            elif not job["containerized"]:
                if job["status"] in JOB_FINISHED_STATES:
                    continue
                else:
                    if job["mode"] == "REPORT":
                        await finalize_job(job, "COMPLETED", case_id=case_id)
                    else:
                        await finalize_job(job, "ERROR", "Examination got closed", case_id=case_id)
        return await es_connector.put_examination_state(examination_id=examination_id, state=ExaminationState.CLOSED)

    @app.get(
        "/examinations/{examination_id}/frontend-token",
        tags=["examinations"],
        status_code=200,
        responses={200: {"model": FrontendToken}},
    )
    async def _(
        examination_id: str = params.ex_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        raw_examination = await es_connector.fetch_raw_examination(examination_id)
        app_id = raw_examination["app_id"]

        app_ui_url = await mps_connector.get_app_ui_url(app_id)
        payload = {"app_id": app_id, "app_ui_url": app_ui_url}

        access_token = access_token_tools.create_token_from_dict(
            payload=payload, expires_after_seconds=settings.frontend_token_exp
        )
        return FrontendToken(access_token=access_token)

    @app.get(
        "/examinations/{examination_id}/app-ui-config",
        tags=["examinations"],
        status_code=200,
        responses={200: {"model": AppUiConfiguration}},
    )
    async def _(
        examination_id: str = params.ex_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        raw_examination = await es_connector.fetch_raw_examination(examination_id)
        app_id = raw_examination["app_id"]
        app_ui_config = await mps_connector.get_app_ui_config(app_id=app_id)

        if app_ui_config is None:
            raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail="No app UI configuration for app.")

        return app_ui_config

    @app.put(
        "/examinations/{examination_id}/scope",
        tags=["examinations"],
        responses={
            200: {"model": ScopeTokenAndScopeID},
            201: {"model": ScopeTokenAndScopeID},
            400: {"model": Message},
        },
    )
    async def _(
        examination_id: str = params.ex_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        status_code, raw_scope = await es_connector.fetch_or_create_raw_scope(
            examination_id=examination_id, user_id=user_id
        )
        raw_scope_token = await es_connector.fetch_raw_scope_token(raw_scope["id"])
        return JSONResponse(
            status_code=status_code,
            content={"scope_id": raw_scope["id"], "access_token": raw_scope_token["access_token"]},
        )

    @app.put(
        "/examinations/query",
        tags=["examinations"],
        status_code=200,
        responses={200: {"model": ExaminationList}},
    )
    async def _(
        query: ExaminationQuery,
        skip: int = None,
        limit: int = None,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        # this endpoint does not retrieve data itself, just IDs - no auth hook performed
        return await es_connector.fetch_examinations(cases=query.cases, skip=skip, limit=limit)

    @app.put(
        "/examinations",
        tags=["examinations"],
        status_code=201,
        responses={200: {"model": Examination}, 201: {"model": Examination}},
    )
    async def _(
        response: Response,
        examination: PostExamination,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        app_id = examination.app_id
        case_id = examination.case_id
        await api_integration.user_case_hook(user_id=user_id, case_id=case_id, auth_payload=payload)
        await cds_connector.fetch_raw_case(case_id=case_id, with_slides=False)  # only check if case exists
        await mps_connector.fetch_app_view(app_id)  # check app exists
        status, raw_examination = await es_connector.fetch_or_create_raw_open_examination(case_id, app_id, user_id)
        response.status_code = status
        examination_id = raw_examination["id"]
        examination = await es_connector.fetch_examination(examination_id=examination_id)
        await api_integration.user_post_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        return examination
