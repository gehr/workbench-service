from .routes import cases, examinations, frontends, scopes, slides


def add_routes_v2(app, late_init):
    cases.add_routes(app, late_init)
    slides.add_routes(app, late_init)
    examinations.add_routes(app, late_init)


def add_routes_v2_scopes(app, late_init):
    scopes.add_routes(app, late_init)


def add_routes_v2_frontends(app, late_init):
    frontends.add_routes(app, late_init)
