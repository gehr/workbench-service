from fastapi import Path, Query

# ## Query params
# example will be pre filled out in swagger ui
image_format = Query(
    "jpeg",
    description="Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.",
    example="jpeg",
)
image_quality = Query(
    90,
    description="Image quality (Only for specific formats. For Jpeg files compression is always lossy. \
        For tiff files 'deflate' compression is used by default. Set to 0 to compress lossy with 'jpeg')",
    example=99,
)
image_channels = Query(
    None,
    description="List of requested image channels. By default all channels are returned.",
)
padding_color = Query(
    None,
    description="Padding color as 24-bit hex-string",
    example="#FFFFFF",
)
z = Query(
    None,
    description="Z coordinate / stack",
    example=0,
)

# ## Path params
# example will be pre filled out in swagger ui
plis_case_id = Path(
    description="(A)PLIS case ID to be resolved to local EMPAIA ID",
    example="a8ba5ed5-997e-4dad-a8da-8b42a46752e7",
)
plis_slide_id = Path(
    description="(A)PLIS slide ID to be resolved to local EMPAIA ID",
    example="6e57df5e-4b59-4a79-97fa-580e4425bea4",
)
case_id = Path(
    description="EMPAIA case ID",
    example="a8ba5ed5-997e-4dad-a8da-8b42a46752e7",
)
ex_id = Path(
    description="Examination ID",
    example="a8ba5ed5-997e-4dad-a8da-8b42a46752e7",
)
job_id = Path(
    description="Job ID",
    example="a8ba5ed5-997e-4dad-a8da-8b42a46752e7",
)
app_id = Path(
    description="App ID",
    example="a8ba5ed5-997e-4dad-a8da-8b42a46752e7",
)
annotation_id = Path(
    description="Annotation ID",
    example="b10648a7-340d-43fc-a2d9-4d91cc86f33f",
)
primitive_id = Path(
    description="Primitive ID",
    example="b10648a7-340d-43fc-a2d9-4d91cc86f33f",
)
class_id = Path(
    description="Class ID",
    example="b10648a7-340d-43fc-a2d9-4d91cc86f33f",
)
collection_id = Path(
    description="Collection ID",
    example="b10648a7-340d-43fc-a2d9-4d91cc86f33f",
)
item_id = Path(
    description="Collection Item ID",
    example="b10648a7-340d-43fc-a2d9-4d91cc86f33f",
)
slide_id = Path(
    description="Slide ID",
    example="6e57df5e-4b59-4a79-97fa-580e4425bea4",
)
level = Path(
    description="Zoom level",
    example=0,
)
max_x = Path(
    description="Maximum width of image",
    example=100,
)
max_y = Path(
    description="Maximum height of image",
    example=100,
)
node_id = Path(
    description="Results view node id",
    example="b10648a7-340d-43fc-a2d9-4d91cc86f33f",
)
tile_x = Path(
    description="Tile number horizontally",
    example=100,
)
tile_y = Path(
    description="Tile number vertically",
    example=100,
)
start_x = Path(
    description="Upper left x coordinate",
    example=100,
)
start_y = Path(
    description="Upper left y coordinate",
    example=100,
)
size_x = Path(
    description="Width",
    example=200,
)
size_y = Path(
    description="Height",
    example=200,
)
