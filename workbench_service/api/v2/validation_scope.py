import logging
from http import HTTPStatus
from time import time

from fastapi import Depends, HTTPException, Request
from fastapi.security import HTTPBearer

from ...models.v1.commons import Id
from .connectors import es_connector
from .validation_helpers import validate_token

logger = logging.getLogger("uvicorn")


class ScopeTokenValidator:
    def __init__(self, key_refresh_interval: int):
        self._verification_key: str = None
        self._key_refresh_interval = key_refresh_interval
        self._last_key_refresh_time: float = None

    async def _ensure_fresh_verification_key(self):
        # The verification key must be regularly refreshed in case the examination service updates it,
        # otherwise the scope token validation will fail.
        if (
            None in (self._verification_key, self._last_key_refresh_time)
            or (time().real - self._last_key_refresh_time) > self._key_refresh_interval
        ):
            try:
                self._verification_key = await es_connector.fetch_raw_public_key()
            except HTTPException as e:
                logger.error(
                    "Could not retrieve public key for scope token validation from examination-service "
                    f"(http status: {e.status_code}):\n{e.detail}"
                )
            except Exception as e:
                logger.error(f"Could not retrieve public key for scope token validation from examination-service: {e}")
            else:
                self._last_key_refresh_time = time().real

    async def __call__(self, request: Request, scope_id: Id, credentials=Depends(HTTPBearer())):
        scope_access_token = credentials.credentials
        await self._validate_token(scope_access_token, scope_id)

    async def _get_verification_key(self) -> bytes:
        await self._ensure_fresh_verification_key()
        return self._verification_key

    async def _validate_token(self, access_token: str, scope_id: Id):
        key = await self._get_verification_key()
        if key is None:
            raise HTTPException(
                status_code=401,
                detail="Auth not initialized on server.",
            )
        payload = await validate_token(access_token, key)
        self._validate_claims(payload, scope_id)
        return payload

    @staticmethod
    def _validate_claims(payload: dict, scope_id: Id):
        if "sub" not in payload or payload["sub"] != scope_id:
            raise HTTPException(status_code=HTTPStatus.PRECONDITION_FAILED)


class ScopeValidation:
    def __init__(self, api_integration):
        # with no_auth deployment, there are no auth settings and no refresh interval,
        # use a default of 300 seconds then:
        auth_settings = getattr(api_integration, "auth_settings", None)
        key_refresh_interval = getattr(auth_settings, "refresh_interval", 300)
        self.scope_token_validator = ScopeTokenValidator(key_refresh_interval)

    def scope_depends(self):
        return Depends(self.scope_token_validator)
