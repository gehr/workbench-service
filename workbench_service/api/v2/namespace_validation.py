import json
import pkgutil

namespace_files = ["org.empaia.global.v1.json"]


def get_global_namespaces():
    global_namespaces = {}
    for file in namespace_files:
        data = pkgutil.get_data(__name__, f"../../definitions/namespaces/{file}").decode()
        data_dict = json.loads(data)
        global_namespaces[data_dict["namespace"]] = {"classes": data_dict["classes"]}
    return global_namespaces


def get_ead_namespace(ead):
    namespace = ead.get("namespace")
    classes = ead.get("classes")
    return {namespace: {"classes": classes}}


def parse_class_values(base_namespace, namespace):
    classes = []
    for key, data in namespace.items():
        value = f"{base_namespace}.{key}"
        if "name" in data:
            classes.append(value)
        else:
            sub_classes = parse_class_values(value, data)
            classes.extend(sub_classes)
    return classes


def flatten_class_namespaces(namespaces):
    global_classes = []
    for namespace, cls in namespaces.items():
        base_namespace = f"{namespace}.classes"
        if "classes" in cls and cls["classes"]:
            classes = parse_class_values(base_namespace, cls["classes"])
            global_classes.extend(classes)
    return global_classes


def validate_class_value(class_value, class_namespaces):
    class_values = flatten_class_namespaces(class_namespaces)
    return class_value in class_values
