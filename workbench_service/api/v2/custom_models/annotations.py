from typing import Dict

from workbench_service.models.commons import RestrictedBaseModel

ROI_CLASS_VALUE = "org.empaia.global.v1.classes.roi"


class ClassesDict(RestrictedBaseModel):
    classes: Dict
