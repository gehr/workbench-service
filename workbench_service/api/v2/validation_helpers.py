import jwt
from fastapi import HTTPException


async def validate_token(access_token, key):
    try:
        payload = jwt.decode(access_token, key=key, algorithms="RS256")
    except jwt.exceptions.ExpiredSignatureError as e:
        raise HTTPException(status_code=401, detail="Access Token expired.") from e
    except jwt.exceptions.InvalidTokenError as e:
        raise HTTPException(status_code=401, detail="Error decoding Token.") from e
    return payload
