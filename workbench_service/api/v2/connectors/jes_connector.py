from ....singletons import http_client, jes_url, settings


async def stop_job_execution(job_id):
    url = f"{jes_url}/v1/executions/{job_id}/stop"
    return await http_client.put(url, headers={"organization-id": settings.organization_id})
