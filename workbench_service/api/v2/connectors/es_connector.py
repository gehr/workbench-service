import uuid
from http import HTTPStatus
from typing import List, Tuple

from fastapi.exceptions import HTTPException

from ....models.v1.commons import Id
from ....models.v1.examination import PostScope
from ....models.v1.job import Job
from ....singletons import http_client, mds_url
from ..custom_models.apps import AppList, PostAppResponse
from ..custom_models.examinations import Examination, ExaminationList, ExaminationState
from ..custom_models.jobs import JOB_FINISHED_STATES
from . import js_connector, mps_connector


async def aggregate_examination(raw_examination: dict, allowed_app_ids: list) -> Tuple[Examination, List[Job]]:
    jobs_count = 0
    jobs_count_finished = 0
    jobs = []
    job_ids = []
    app_ids = []

    for app in raw_examination["apps"]:
        if app["id"] in allowed_app_ids:
            app_ids.append(app["id"])
            job_ids += app["jobs"]

    if len(job_ids) > 0:
        raw_jobs = await js_connector.fetch_raw_jobs(jobs=job_ids)
        for raw_job in raw_jobs["items"]:
            job = Job.model_validate(raw_job)
            jobs.append(job)

            jobs_count += 1
            if job.status in JOB_FINISHED_STATES:
                jobs_count_finished += 1

    created_at = raw_examination["created_at"]
    updated_at = raw_examination["updated_at"]

    examination = Examination(
        id=raw_examination["id"],
        case_id=raw_examination["case_id"],
        creator_id=raw_examination["creator_id"],
        creator_type=raw_examination["creator_type"],
        state=raw_examination["state"],
        created_at=created_at,
        updated_at=updated_at,
        jobs=job_ids,
        jobs_count=jobs_count,
        jobs_count_finished=jobs_count_finished,
        apps=app_ids,
    )
    return examination, jobs


async def fetch_raw_examination(examination_id: str) -> dict:
    url = f"{mds_url}/v1/examinations/{examination_id}"
    raw_examination = await http_client.get(url)
    return raw_examination


async def fetch_jobs(examination_id: str, app_id: str) -> List[str]:
    raw_examination = await fetch_raw_examination(examination_id=examination_id)
    for app in raw_examination["apps"]:
        if app["id"] == app_id:
            return app["jobs"]
    return None


async def check_app_in_examination(examination_id: str, app_id: str) -> dict:
    raw_examination = await fetch_raw_examination(examination_id=examination_id)
    for app in raw_examination["apps"]:
        if app["id"] == app_id:
            return True
    raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail="App not found in examination.")


async def fetch_examination(examination_id: str) -> Tuple[Examination, List[Job]]:
    raw_examination = await fetch_raw_examination(examination_id=examination_id)
    app_ids = [a["id"] for a in raw_examination["apps"]]
    query = {"apps": app_ids}
    raw_app_views = await mps_connector.query_app_views(query)
    app_ids = [a["app"]["id"] for a in raw_app_views]
    examination, jobs = await aggregate_examination(raw_examination=raw_examination, allowed_app_ids=app_ids)
    return examination, jobs


async def fetch_raw_examinations(cases: List[str] = None, skip: int = None, limit: int = None) -> dict:
    url = f"{mds_url}/v1/examinations/query"
    raw_query = {}
    if cases:
        raw_query["cases"] = cases
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    raw_examinations = await http_client.put(url, json=raw_query, params=params)
    return raw_examinations


async def fetch_examinations(cases: List[str] = None, skip: int = None, limit: int = None) -> ExaminationList:
    raw_examinations = await fetch_raw_examinations(cases=cases, skip=skip, limit=limit)

    app_ids = []
    for r_ex in raw_examinations["items"]:
        app_ids += [a["id"] for a in r_ex["apps"]]
    query = {"apps": app_ids}
    raw_app_views = await mps_connector.query_app_views(query)
    app_ids = [a["app"]["id"] for a in raw_app_views]

    examinations = []
    for raw_examination in raw_examinations["items"]:
        examination, _jobs = await aggregate_examination(raw_examination=raw_examination, allowed_app_ids=app_ids)
        examinations.append(examination)
    examinations = ExaminationList(items=examinations, item_count=raw_examinations["item_count"])
    return examinations


async def get_ex_and_job_counts(case_id: str):
    raw_examinations = await fetch_raw_examinations(cases=[case_id])

    ex_count = raw_examinations["item_count"]
    jobs_count = 0
    jobs_count_finished = 0
    job_ids = []

    for raw_examination in raw_examinations["items"]:
        for app in raw_examination["apps"]:
            job_ids += app["jobs"]

    if len(job_ids) > 0:
        raw_jobs = await js_connector.fetch_raw_jobs(job_ids)

        for raw_job in raw_jobs["items"]:
            job = Job.model_validate(raw_job)

            jobs_count += 1
            if job.status in JOB_FINISHED_STATES:
                jobs_count_finished += 1

    return ex_count, jobs_count, jobs_count_finished


async def post_examination(case_id: str, user_id: str) -> Examination:
    examinations = await fetch_examinations(cases=[case_id])
    for ex in examinations.items:
        if ex.state == ExaminationState.OPEN:
            raise HTTPException(400, f"Another open examination found in case '{case_id}'")

    url = f"{mds_url}/v1/examinations"
    payload = {"case_id": case_id, "creator_id": user_id, "creator_type": "USER"}
    raw_examination = await http_client.post(url, json=payload)
    # new examination has no apps yet, we therefore can pass empty list as allowed app_ids for filtering
    examination, _jobs = await aggregate_examination(raw_examination=raw_examination, allowed_app_ids=[])
    return examination


async def put_examination_app(examination_id: str, app_id: str) -> PostAppResponse:
    url = f"{mds_url}/v1/examinations/{examination_id}/apps/{app_id}/add"
    # add app-id to examination
    raw_examination = await http_client.put(url)
    return PostAppResponse(examination_id=raw_examination["id"], app_id=app_id)


async def fetch_examination_apps(examination_id: str) -> AppList:
    examination, jobs = await fetch_examination(examination_id=examination_id)
    aggregated_apps = await mps_connector.fetch_aggregated_apps({"apps": examination.apps})
    aggregated_apps_dict = dict([(app.id, app) for app in aggregated_apps.items if app.id in examination.apps])

    for app_id in aggregated_apps_dict:
        all_relevant_jobs = [j for j in jobs if str(j.app_id) == app_id]
        finished_jobs = [j for j in all_relevant_jobs if j.status in JOB_FINISHED_STATES]

        aggregated_apps_dict[app_id].jobs_count = len(all_relevant_jobs)
        aggregated_apps_dict[app_id].jobs_count_finished = len(finished_jobs)

    apps = AppList(
        items=list(aggregated_apps_dict.values()),
        item_count=len(aggregated_apps_dict),
    )
    return apps


async def put_examination_state(examination_id: str, state: ExaminationState) -> dict:
    _, jobs = await fetch_examination(examination_id=examination_id)
    # check if all jobs are in a finished state
    for job in jobs:
        if job.status not in JOB_FINISHED_STATES and state == ExaminationState.CLOSED:
            raise HTTPException(400, f"At least one job in examination '{examination_id}' is in a not finished state")
    url = f"{mds_url}/v1/examinations/{examination_id}/state"
    payload = {"state": state.value}
    raw_examination = await http_client.put(url, json=payload)
    return raw_examination


async def fetch_or_create_raw_scope(examination_id: Id, app_id: Id, user_id: Id) -> Tuple[int, dict]:
    url = f"{mds_url}/v1/scopes"
    try:
        examination_id = uuid.UUID(examination_id, version=4)
    except ValueError as e:
        raise HTTPException(422, "no valid UUID4") from e
    post_scope = PostScope(examination_id=examination_id, app_id=app_id, user_id=user_id)
    payload = post_scope.model_dump()
    raw_scope = None
    try:
        raw_scope = await http_client.put(url, json=payload)
        status_code = 200
    except HTTPException as e:
        if e.status_code != 404:
            raise
    if raw_scope is None:
        try:
            raw_scope = await http_client.post(url, json=payload)
            status_code = 201
        except HTTPException as e:
            # in case scope creation fails due to parallel requests
            if e.status_code == 400:
                raw_scope = await http_client.put(url, json=payload)
                status_code = 200
            else:
                raise
    return status_code, raw_scope


async def fetch_raw_scope_token(scope_id: Id) -> dict:
    url = f"{mds_url}/v1/scopes/{scope_id}/token"
    return await http_client.get(url)


async def fetch_raw_scope_data(scope_id: Id) -> dict:
    url = f"{mds_url}/v1/scopes/{scope_id}"
    return await http_client.get(url)


async def fetch_raw_public_key() -> bytes:
    url = f"{mds_url}/v1/examination-service/public-key"
    status_code, public_key = await http_client.get(url, return_status=True)
    if status_code < 400:
        public_key = public_key.encode("ascii")
    else:
        raise HTTPException(status_code=status_code, detail="Failed to retrieve public-key from examination-service")
    return public_key


async def fetch_case_id_for_scope(scope_data: dict) -> str:
    ex_data = await fetch_raw_examination(scope_data["examination_id"])
    return ex_data["case_id"]


async def put_examination_job(ex_id, app_id, job_id):
    result = await http_client.put(
        f"{mds_url}/v1/examinations/{ex_id}/apps/{app_id}/jobs/{job_id}/add",
        None,
    )
    return result


async def delete_examination_job(ex_id, job_id):
    url = f"{mds_url}/v1/examinations/{ex_id}/jobs/{job_id}"
    return await http_client.delete(url)


async def validate_examination_state_open(scope_id):
    scope_data = await fetch_raw_scope_data(scope_id)
    raw_examination = await fetch_raw_examination(examination_id=scope_data["examination_id"])
    if raw_examination["state"] == ExaminationState.CLOSED:
        raise HTTPException(
            status_code=423, detail="Examination has state 'CLOSED': Create or delete actions are not allowed"
        )
    return raw_examination
