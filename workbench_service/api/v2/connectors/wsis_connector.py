from typing import List

from fastapi.exceptions import HTTPException
from fastapi.responses import StreamingResponse

from ....models.v1.commons import Id
from ....models.v1.slide import SlideInfo
from ....singletons import http_client, mds_url
from . import cds_connector, es_connector


async def fetch_slide_info(slide_id: str) -> SlideInfo:
    url = f"{mds_url}/v1/slides/{slide_id}/info"
    raw_slide_info = await http_client.get(url)
    return SlideInfo(
        id=raw_slide_info["id"],
        channels=raw_slide_info.get("channels"),
        channel_depth=raw_slide_info.get("channel_depth"),
        extent=raw_slide_info["extent"],
        num_levels=raw_slide_info["num_levels"],
        pixel_size_nm=raw_slide_info["pixel_size_nm"],
        tile_extent=raw_slide_info["tile_extent"],
        levels=raw_slide_info["levels"],
    )


async def fetch_raw_stream_slide_tile(
    slide_id: str,
    level: int,
    tile_x: int,
    tile_y: int,
    image_format: str = "jpg",
    image_quality: int = 90,
    image_channels: List[int] = None,
    padding_color: str = "#FFFFFF",
    z: int = 0,
) -> StreamingResponse:
    url = f"{mds_url}/v1/slides/{slide_id}/tile" f"/level/{level}/tile/{tile_x}/{tile_y}"
    params = {
        "image_format": image_format,
        "image_quality": image_quality,
        "image_channels": image_channels,
        "padding_color": padding_color,
        "z": z,
    }
    return await http_client.get_stream_response(url, params)


async def fetch_raw_stream_slide_region(
    slide_id: str,
    level: int,
    start_x: int,
    start_y: int,
    size_x: int,
    size_y: int,
    image_format: str = "jpg",
    image_quality: int = 90,
    image_channels: List[int] = None,
    z: int = 0,
) -> StreamingResponse:
    url = f"{mds_url}/v1/slides/{slide_id}/region" f"/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}"
    params = {
        "image_format": image_format,
        "image_quality": image_quality,
        "image_channels": image_channels,
        "z": z,
    }
    return await http_client.get_stream_response(url, params)


async def fetch_raw_stream_slide_label(
    slide_id: str,
    max_x: int,
    max_y: int,
    image_format: str = "jpg",
    image_quality: int = 90,
) -> StreamingResponse:
    url = f"{mds_url}/v1/slides/{slide_id}/label/max_size/{max_x}/{max_y}"
    params = {
        "image_format": image_format,
        "image_quality": image_quality,
    }
    return await http_client.get_stream_response(url, params)


async def fetch_raw_stream_slide_macro(
    slide_id: str,
    max_x: int,
    max_y: int,
    image_format: str = "jpg",
    image_quality: int = 90,
) -> StreamingResponse:
    url = f"{mds_url}/v1/slides/{slide_id}/macro/max_size/{max_x}/{max_y}"
    params = {
        "image_format": image_format,
        "image_quality": image_quality,
    }
    return await http_client.get_stream_response(url, params)


async def fetch_raw_stream_slide_thumbnail(
    slide_id: str,
    max_x: int,
    max_y: int,
    image_format: str = "jpg",
    image_quality: int = 90,
) -> StreamingResponse:
    url = f"{mds_url}/v1/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}"
    params = {
        "image_format": image_format,
        "image_quality": image_quality,
    }
    return await http_client.get_stream_response(url, params)


async def fetch_raw_slides_for_scope(scope_data: dict) -> dict:
    case_id = await es_connector.fetch_case_id_for_scope(scope_data)
    if case_id:
        slide_data = await cds_connector.fetch_raw_slides([case_id])
        return slide_data
    else:
        raise HTTPException(status_code=404, detail="No valid case ID for scope")


async def validate_scope_slide(scope_id: Id, slide_id: str) -> bool:
    scope_data = await es_connector.fetch_raw_scope_data(scope_id)
    slide_data = await fetch_raw_slides_for_scope(scope_data)

    for slide in slide_data["items"]:
        if slide["id"] == slide_id:
            return True

    raise HTTPException(status_code=404, detail="Requested slide ID does not exist for current scope")
