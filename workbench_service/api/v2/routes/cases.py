from fastapi import Header

from .. import params
from ..connectors import cds_connector, es_connector, mps_connector
from ..custom_models.apps import AppList
from ..custom_models.cases import Case, CaseList
from ..custom_models.examinations import Examination, ExaminationList
from ..custom_models.slides import SlideList
from ..singletons import api_integration


def add_routes(app, late_init):
    @app.get(
        "/cases",
        tags=["cases panel"],
        status_code=200,
        responses={200: {"model": CaseList}},
    )
    async def _(
        skip: int = None,
        limit: int = None,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        return await cds_connector.fetch_cases(skip=skip, limit=limit)

    @app.get(
        "/cases/{case_id}",
        tags=["cases panel"],
        status_code=200,
        responses={200: {"model": Case}},
    )
    async def _(
        user_id: str = Header(...),
        case_id: str = params.case_id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_case_hook(user_id=user_id, case_id=case_id, auth_payload=payload)
        return await cds_connector.fetch_case(case_id=case_id)

    @app.get(
        "/cases/{case_id}/slides",
        tags=["cases panel"],
        status_code=200,
        responses={200: {"model": SlideList}},
    )
    async def _(
        skip: int = None,
        limit: int = None,
        user_id: str = Header(...),
        case_id: str = params.case_id,
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_case_hook(user_id=user_id, case_id=case_id, auth_payload=payload)
        await cds_connector.fetch_raw_case(case_id=case_id, with_slides=False)  # only check if case exists
        return await cds_connector.fetch_slides(case_id=case_id, skip=skip, limit=limit)

    @app.get(
        "/cases/{case_id}/examinations",
        tags=["cases panel"],
        status_code=200,
        responses={200: {"model": ExaminationList}},
    )
    async def _(
        case_id: str = params.case_id,
        skip: int = None,
        limit: int = None,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_case_hook(user_id=user_id, case_id=case_id, auth_payload=payload)
        await cds_connector.fetch_raw_case(case_id=case_id, with_slides=False)  # only check if case exists
        return await es_connector.fetch_examinations(cases=[case_id], skip=skip, limit=limit)

    @app.post(
        "/cases/{case_id}/examinations",
        tags=["cases panel"],
        status_code=201,
        responses={201: {"model": Examination}},
    )
    async def _(
        case_id: str = params.case_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_case_hook(user_id=user_id, case_id=case_id, auth_payload=payload)
        await cds_connector.fetch_raw_case(case_id=case_id, with_slides=False)  # only check if case exists
        return await es_connector.post_examination(case_id=case_id, user_id=user_id)

    @app.get(
        "/cases/{case_id}/apps",
        tags=["cases panel"],
        response_model=AppList,
    )
    async def _(
        case_id: str = params.case_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_case_hook(user_id=user_id, case_id=case_id, auth_payload=payload)
        await cds_connector.fetch_raw_case(case_id=case_id, with_slides=False)  # only check if case exists
        return await mps_connector.fetch_aggregated_apps(query={})
