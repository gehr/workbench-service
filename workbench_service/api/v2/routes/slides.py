from typing import List

from fastapi import Header
from fastapi.responses import StreamingResponse

from ....models.v1.slide import SlideInfo
from .. import params
from ..connectors import wsis_connector
from ..singletons import api_integration

ImageResponses = {
    200: {
        "content": {
            "image/*": {"schema": {"type": "string", "format": "binary"}},
        },
        "description": "The image response. Format (e.g. png, jpeg) according to used query parameter [image_format]",
    },
}


def add_routes(app, late_init):
    @app.get(
        "/slides/{slide_id}/info",
        tags=["slides panel"],
        response_model=SlideInfo,
    )
    async def _(
        slide_id: str = params.slide_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_slide_hook(user_id=user_id, slide_id=slide_id, auth_payload=payload)
        return await wsis_connector.fetch_slide_info(slide_id)

    @app.get(
        "/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}",
        tags=["slides panel"],
        responses=ImageResponses,
        response_class=StreamingResponse,
    )
    async def _(
        slide_id: str = params.slide_id,
        level: int = params.level,
        tile_x: int = params.tile_x,
        tile_y: int = params.tile_y,
        image_format: str = params.image_format,
        image_quality: int = params.image_quality,
        image_channels: List[int] = params.image_channels,
        padding_color: str = params.padding_color,
        z: int = params.z,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_slide_hook(user_id=user_id, slide_id=slide_id, auth_payload=payload)
        return await wsis_connector.fetch_raw_stream_slide_tile(
            slide_id,
            level,
            tile_x,
            tile_y,
            image_format,
            image_quality,
            image_channels,
            padding_color,
            z,
        )

    @app.get(
        "/slides/{slide_id}/macro/max_size/{max_x}/{max_y}",
        tags=["slides panel"],
        responses=ImageResponses,
        response_class=StreamingResponse,
    )
    async def _(
        slide_id: str = params.slide_id,
        max_x: int = params.max_x,
        max_y: int = params.max_y,
        image_format: str = params.image_format,
        image_quality: int = params.image_quality,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_slide_hook(user_id=user_id, slide_id=slide_id, auth_payload=payload)
        return await wsis_connector.fetch_raw_stream_slide_macro(slide_id, max_x, max_y, image_format, image_quality)

    @app.get(
        "/slides/{slide_id}/label/max_size/{max_x}/{max_y}",
        tags=["slides panel"],
        responses=ImageResponses,
        response_class=StreamingResponse,
    )
    async def _(
        slide_id: str = params.slide_id,
        max_x: int = params.max_x,
        max_y: int = params.max_y,
        image_format: str = params.image_format,
        image_quality: int = params.image_quality,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_slide_hook(user_id=user_id, slide_id=slide_id, auth_payload=payload)
        return await wsis_connector.fetch_raw_stream_slide_label(slide_id, max_x, max_y, image_format, image_quality)

    @app.get(
        "/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}",
        tags=["slides panel"],
        responses=ImageResponses,
        response_class=StreamingResponse,
    )
    async def _(
        slide_id: str = params.slide_id,
        max_x: int = params.max_x,
        max_y: int = params.max_y,
        image_format: str = params.image_format,
        image_quality: int = params.image_quality,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_slide_hook(user_id=user_id, slide_id=slide_id, auth_payload=payload)
        return await wsis_connector.fetch_raw_stream_slide_thumbnail(
            slide_id, max_x, max_y, image_format, image_quality
        )
